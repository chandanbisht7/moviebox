import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SearchMovieService } from '../../services/search-movie.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';


export class Movie {
  public name: string | undefined;
  public type : string | undefined;
  
}

@Component({
  selector: 'app-search-movie',
  templateUrl: './search-movie.component.html',
  styleUrls: ['./search-movie.component.css']
})
export class SearchMovieComponent implements OnInit {
  myForm!: FormGroup ;
  searchType: any = ["Title", "Imdb ID"]
  model=new Movie();
  movieYear: any;
  movie: any={};
  genre: any;
  isCompleted=true;
  isDataPresent=false;

  constructor(private searchService: SearchMovieService, private snackbar: MatSnackBar) {

   }
   
   public myError = (controlName: string, errorName: string) =>{
    return this.myForm?.controls[controlName].hasError(errorName);
    }
    
  ngOnInit(): void {

    this.myForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      type: new FormControl('', [Validators.required]),
    
      });
      
    this.isCompleted=false;
  }

  searchMovie() {
    this.isCompleted=true;
    //call the service pass search item and type
    if(this.myForm.valid){
    let type=this.myForm.controls['type'].value;
    let name=this.myForm.controls['name'].value;
    this.searchService.search(type,name).subscribe((result) => {
      console.log(result);
      if (result != undefined && result != "") {
        if(result.Response !="False"){
        //show message to user
        this.isDataPresent=true;
        this.snackbar.open("Data retrived successfully", '', {
          duration: 1000,
          verticalPosition: 'top',
          horizontalPosition: 'center',
          panelClass: 'snackbar'
        });
        this.movie=result;
        this.genre = this.movie.Genre.split(',')
      }
      else{
        this.isDataPresent=false
        this.snackbar.open(result.Error, '', {
          duration: 1000,
          verticalPosition: 'top',
          horizontalPosition: 'center',
          panelClass: 'snackbar'
        });
      }
      }
      else {
        this.isDataPresent=false
        this.snackbar.open("No data present", '', {
          duration: 1000,
          verticalPosition: 'top',
          horizontalPosition: 'center',
          panelClass: 'snackbar'
        });
        
      }
      this.isCompleted=false;
    });
  }
  else{
    this.isDataPresent=false;
    this.snackbar.open("Please enter required fields", '', {
      duration: 1000,
      verticalPosition: 'top',
      horizontalPosition: 'center',
      panelClass: 'snackbar'
    });
    this.isCompleted=false;
  }

  }
}
