import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { SearchMovieComponent } from './components/search-movie/search-movie.component';

const routes: Routes = [
{ path: "", component:SearchMovieComponent },
{ path: "search-movie", component: SearchMovieComponent},
{ path: "about", component: AboutComponent},
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovieRoutingModule { }
