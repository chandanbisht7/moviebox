import { Injectable } from '@angular/core';
import { GlobalConstants } from '../global-constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
const headers = new HttpHeaders().set('Content-Type', 'application/X-www-form-urlencoded');

@Injectable({
  providedIn: 'root'
})

export class SearchMovieService {
  public result: any;

  constructor(private http: HttpClient) { }

  apiURL=GlobalConstants.apiURL;
  key=GlobalConstants.key;

  search(type:string,name:string): Observable<any> {
   
    try {
      let apiPath="";
      if(type=="Title")
      {
        apiPath= this.apiURL+"?apikey="+this.key+"&t="+ name;
      }
      else{
        apiPath= this.apiURL+"?apikey="+this.key+"&i="+ name;
      }
      this.result = this.http.get(apiPath);
      return this.result;
    } catch (e) {
      console.log(e, 'error')
    }
    return this.result;
  }


}
